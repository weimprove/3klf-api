import { apiStatus } from '../../../lib/util'
import { Router } from 'express'

const Magento2Client = require('magento2-rest-client').Magento2Client

module.exports = ({ config, db }) => {
  let mcApi = Router()

  const client = Magento2Client(config.magento2.api)

  client.addMethods('events', (restClient) => ({
    customerEvents: customerToken => restClient.get('/event/mine', customerToken)
  }))

  mcApi.get('/customer', (req, res) => {
    client.events.customerEvents(req.query.token).then((result) => {
      apiStatus(res, result, 200)
    }).catch(err => {
      apiStatus(res, err, 500)
    })
  })

  return mcApi
}
